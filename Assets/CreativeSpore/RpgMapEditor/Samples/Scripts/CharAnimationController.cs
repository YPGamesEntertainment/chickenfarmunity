﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

namespace CreativeSpore
{
	public class CharAnimationController : MonoBehaviour 
	{
		public enum eDir
		{
			DOWN,
			LEFT,
			RIGHT,
			UP
		}

        public enum eCharSetType
        {
            RPG_Maker_VX,
            RPG_Maker_XP
        }

        Vector3[] m_dirVect = new Vector3[]
        {
            new Vector3(0, -1), //DOWN
            new Vector3(-1, 0), //LEFT
            new Vector3(1, 0), //RIGHT
            new Vector3(0, 1), //UP
        };

		public Sprite SpriteCharSet;
		public SpriteRenderer TargetSpriteRenderer;
        public Vector2[] Pivot = null;
		public eDir CurrentDir
        {
            get { return m_currentDir; }
            set 
            {
                bool hasChanged = m_currentDir != value;
                m_currentDir = value;
                if (hasChanged)
                {
                    if (m_charsetType == eCharSetType.RPG_Maker_XP)
                        TargetSpriteRenderer.sprite = m_spriteXpIdleFrames[(int)m_currentDir];
                    else
                        TargetSpriteRenderer.sprite = m_spriteFrames[(int)((int)m_currentDir * AnimFrames)];
                }
            }
        }
        public Vector3 CurrentDirVect { get { return m_dirVect[(int)CurrentDir]; } }
        public bool IsPingPongAnim = true; // set true for ping pong animation
		public bool IsAnimated = true;
		public float AnimSpeed = 9f; // frames per second
        public int AnimFrames
        {
            get { return 3; }
        }
        public eCharSetType CharsetType
        {
            get { return m_charsetType; }
            set 
            {
                bool hasChanged = m_charsetType != value;
                m_charsetType = value;
                if (hasChanged) CreateSpriteFrames(); 
            }
        }

        [SerializeField]
        public int CurrentFrame
        {
            get
            {
                int curFrame = m_internalFrame;
                if (IsPingPongAnim && m_isPingPongReverse)
                {
                    curFrame = AnimFrames - 1 - curFrame;
                }
                return curFrame;
            }
        }

        [SerializeField]
		private List<Sprite> m_spriteFrames = new List<Sprite>();
        [SerializeField]
        private List<Sprite> m_spriteXpIdleFrames = new List<Sprite>(); // RPG Maker XP 전용이라는 점을 유념. VX는 사용하지 않는다
        [SerializeField]
        private eCharSetType m_charsetType = eCharSetType.RPG_Maker_VX;
        [SerializeField]
        private eDir m_currentDir = eDir.DOWN;

        private int m_internalFrame; // frame counter used internally. CurFrame would be the real animation frame
        private float m_curFrameTime;               
        private bool m_isPingPongReverse = false;

		void Awake()
		{
            //if( IsDataBroken() ) //A weird bug happens when changing the spritesheet in the prefab. Sprites looks fine, the UVs looks broken in Down anima frame 2
                CreateSpriteFrames();
		}

		// Use this for initialization
		void Start () 
		{
            if (TargetSpriteRenderer == null)
            {
                TargetSpriteRenderer = GetComponentInChildren<SpriteRenderer>();
            }
		}

        void Update()
        {
            UpdateAnim(Time.deltaTime);
        }

        public void UpdateAnim( float dt )
        {
            if (TargetSpriteRenderer == null || TargetSpriteRenderer.sprite == null)
                return;

            if (IsAnimated)
            {
                if (IsPingPongAnim && (m_internalFrame == 0 || m_internalFrame == (AnimFrames - 1)))
                    // 애니메이션의 첫 번째 프레임과 마지막 프레임에서 두 번 머 무르지 않도록합니다?
                    m_curFrameTime += dt * AnimSpeed * 2f; // avoid stay twice of the time in the first and last frame of the animation
                else
                    m_curFrameTime += dt * AnimSpeed;

                // 현재 프레임 타임이 1 이상이면 들어오는 while 
                // while문이라 조금 무섭네
                // 들어오자마자 m_curFrameTime을 1보다 작게 만들어서 빠져나오게 하는걸 보니 굳이 while을 돌릴 필요는 없어보인다 
                while (m_curFrameTime >= 1f)
                {             
                    m_curFrameTime -= 1f;
                    
                    // 애니메이션 프레임을 증가시키고, % 계산으로 3을 넘어가지 않도록 함 
                    ++m_internalFrame; m_internalFrame %= AnimFrames;
                    if (m_internalFrame == 0)
                    {
                        m_isPingPongReverse = !m_isPingPongReverse;
                    }
                }
            }
            else
            {
                m_internalFrame = 0;
            }

            // GetCurrentSprite() 안에서 m_internalFrame을 사용해서 계산함
            // 일종의 갱신 작업
            TargetSpriteRenderer.sprite = GetCurrentSprite( CurrentDir );
        }

        public Sprite GetCurrentSprite( eDir dir )
        {
            if( !IsAnimated && m_charsetType == eCharSetType.RPG_Maker_XP )
                return m_spriteXpIdleFrames[(int)dir];
            else
                // 현재 보는 방향 * 3이니 0,3,6,9 이렇게 나오게 되고, 진행되는 현재 프레임 카운터 0,1,2을 더해 계산해서 내보낸다 
                return m_spriteFrames[(int)((int)dir * AnimFrames + CurrentFrame)];
        }

        public IList<Sprite> GetSpriteFrames()
        {
            return m_spriteFrames;
        }

        public bool IsDataBroken()
        {
            return 
                (Pivot == null || Pivot.Length != 4 || 
                m_spriteFrames.Count != 4*AnimFrames || 
                (m_charsetType == eCharSetType.RPG_Maker_XP && m_spriteXpIdleFrames.Count != 4) ||
                m_spriteFrames.Contains(null) ||
                m_spriteXpIdleFrames.Contains(null)
            );
        }

		public bool CreateSpriteFrames()
		{
			if( SpriteCharSet != null )
			{
                // 상하좌우 방향 스프라이트 애니메이션에 대한 중심점을 설정함
                if (Pivot == null || Pivot.Length < 4)
                {
                    Pivot = new Vector2[]
                    {
                        new Vector2( 0.5f, 0f ),
                        new Vector2( 0.5f, 0f ),
                        new Vector2( 0.5f, 0f ),
                        new Vector2( 0.5f, 0f )
                    };
                }
                // 에디터를 통해 중심점을 수동으로 잡아줬을 때, 4개 전부를 잡아주지 않았다면 4개로 리사이징 해줌 
                else if( Pivot.Length != 4 )
                {
                    System.Array.Resize<Vector2>(ref Pivot, 4);
                }

                // 재초기화를 위해 호출될 수 있으니 싹다 날린다 
                foreach (Sprite spr in m_spriteFrames) DestroyImmediate(spr);
                m_spriteFrames.Clear();
                m_spriteXpIdleFrames.Clear();

                // 기본 AnimFrames 값은 3, RPG Maker VX는 3프레임이 기본인데, XP는 4프레임이 기본인가봄 
                int animFrames = ( CharsetType == eCharSetType.RPG_Maker_VX? AnimFrames : AnimFrames + 1); // XP characters have 4 frames per side, one is for idle
                int frameWidth = (int)SpriteCharSet.rect.width / animFrames;
                int frameHeight = (int)SpriteCharSet.rect.height / 4;
				int frameNb = 0;

                // 한 프레임에 너비,높이를 이용해서 Rect 생성 
                // 이걸 이용해서 스프라이트에서 하나씩 프레임을 잘라내서 생성하겠지 
				Rect rFrame = new Rect(0f, 0f, (float)frameWidth, (float)frameHeight);

                // 프레임 Rect를 조정할 때 x,y만 조정하고 width, height는 고정이라는 점을 유념하자
                // 아래 계산에 의하면 아래와 같은 프레임 순서대로 스프라이트가 생성된다 
                // 0  1  2
                // 3  4  5
                // 6  7  8
                // 9 10 11
                for (rFrame.y = SpriteCharSet.rect.yMax - frameHeight; rFrame.y >= SpriteCharSet.rect.y; rFrame.y -= frameHeight)
				{
                    for (rFrame.x = SpriteCharSet.rect.x; rFrame.x < SpriteCharSet.rect.xMax; rFrame.x += frameWidth, ++frameNb)
					{
                        try
                        {
                            Sprite sprFrame = Sprite.Create(SpriteCharSet.texture, rFrame, Pivot[frameNb / animFrames], 100f);
                            sprFrame.name = SpriteCharSet.name + "_" + frameNb;
                            if (CharsetType == eCharSetType.RPG_Maker_XP && (frameNb % animFrames == 0))
                                m_spriteXpIdleFrames.Add(sprFrame); // save apart the idle frame for each direction
                            else
                                m_spriteFrames.Add(sprFrame);
                        }
                        catch
                        { 
                            //NOTE: this happens when texture size is not multiple of AnimFrames. In this case, wrong frames are skipped
                            frameNb--;
                        }
					}
				}

                if (TargetSpriteRenderer != null)
				{
					TargetSpriteRenderer.sprite = GetCurrentSprite(CurrentDir);
				}
                return true;
			}
            return false;
		}
	}
}