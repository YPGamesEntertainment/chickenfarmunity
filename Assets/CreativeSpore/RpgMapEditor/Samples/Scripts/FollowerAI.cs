using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using CreativeSpore.RpgMapEditor;

namespace CreativeSpore
{
    [RequireComponent(typeof(MovingBehaviour))]
    [RequireComponent(typeof(PhysicCharBehaviour))]
    [RequireComponent(typeof(MapPathFindingBehaviour))]
    [RequireComponent(typeof(CharAnimationController))]
	public class FollowerAI : MonoBehaviour {

		GameObject m_player;
		MovingBehaviour m_moving;
		PhysicCharBehaviour m_phyChar;
        MapPathFindingBehaviour m_pathFindingBehaviour;
        CharAnimationController m_animCtrl;

        public bool LockAnimDir = false;
        /// <summary>
        /// Distance the object can see the target to follow him
        /// </summary>
        public float SightDistance = 4f;

        /// <summary>
        /// If true, sight line will be blocked by block tiles and enemy won't follow the hidden target
        /// </summary>
        public bool IsSightBlockedByBlockedTiles = true;
        /// <summary>
        /// Distance to stop doing path finding and just go to player position directly
        /// </summary>
        public float MinDistToReachTarget = 0.32f;
		public float AngRandRadious = 0.16f;
		public float AngRandOff = 15f;
		float m_fAngOff;

		// Use this for initialization
		void Start () 
		{
			m_fAngOff = 360f * Random.value;

            // 아래 코드를 통해 알 수 있는 사실은 이 클래스는 그냥 야매 클라스는 점임 
            // 플레이어 단 하나만을 팔로잉 하기 위한 클래스라는 것 
			m_player = GameObject.FindGameObjectWithTag("Player");

			m_moving = GetComponent<MovingBehaviour>();
			m_phyChar = GetComponent<PhysicCharBehaviour>();
            m_pathFindingBehaviour = GetComponent<MapPathFindingBehaviour>();
            m_animCtrl = GetComponent<CharAnimationController>();
		}

#if UNITY_EDITOR
        // 플레이하고 씬뷰를 보면 기즈모를 보여줌 
        // 여기선 AI가 어딜 목적으로 하고 있는지를 기즈모로 보여줬음 
        void OnDrawGizmosSelected()
        {
            Handles.CircleCap(0, transform.position, transform.rotation, SightDistance);
        }
#endif

        void UpdateAnimDir()
        {
            // 무빙 속도의 크기가 무빙 최고 속도의 반보다 크다면? 
            // 일정 속도 이상이여지만 방향을 바꾸는게 말이 되려낭 
            if (m_moving.Veloc.magnitude >= (m_moving.MaxSpeed / 2f))
            {
                if (Mathf.Abs(m_moving.Veloc.x) > Mathf.Abs(m_moving.Veloc.y))
                {
                    if (m_moving.Veloc.x > 0)
                        m_animCtrl.CurrentDir = CharAnimationController.eDir.RIGHT;
                    else if (m_moving.Veloc.x < 0)
                        m_animCtrl.CurrentDir = CharAnimationController.eDir.LEFT;
                }
                else
                {
                    if (m_moving.Veloc.y > 0)
                        m_animCtrl.CurrentDir = CharAnimationController.eDir.UP;
                    else if (m_moving.Veloc.y < 0)
                        m_animCtrl.CurrentDir = CharAnimationController.eDir.DOWN;
                }
            }
        }

		// Update is called once per frame
		void Update() 
		{
            // Update때마다 transform을 불러서 갱신해대네; 
            // 이 Update 함수에는 자신을 움직이게 하는 코드가 없다 
            // m_pathFindingBehaviour 이녀석이 그것을 하는 듯
            // 아래코드처럼 position을 계속 갱신해서 박느니 그냥 m_pathFindingBehaviour가 타겟의 transform을 들게하는게 이득일 듯
            m_pathFindingBehaviour.TargetPos = m_player.transform.position;
            Vector3 vTarget = m_player.transform.position; vTarget.z = transform.position.z;

            Ray2D sightRay = new Ray2D(transform.position, vTarget - transform.position);
            float distToTarget = (vTarget - transform.position).magnitude;

            // 벽 넘어서까지 타겟을 볼 수 있느냐 없느냐
            // 넘어서까지 볼 수 없다면 광선을 쏴서 타겟과 자신의 중앙에 있을 블락타일의 거리를 잼 
            float fSightBlockedDist = IsSightBlockedByBlockedTiles? RpgMapHelper.Raycast(sightRay, distToTarget) : -1f;

            // NOTE: fSightBlockedDist will be -1f if sight line is not blocked by blocked collision tile
            // fSightBlockedDist이 -1이면 블락타일을 무시한다고 함 
            // 타겟과의 거리가 시야보다 크다 or 시야가 블락타일에 차단되고 있다 
            if (distToTarget >= SightDistance || fSightBlockedDist >= 0f)
            {
                // Move around
                m_pathFindingBehaviour.enabled = false;
                vTarget = transform.position;
                m_fAngOff += Random.Range(-AngRandOff, AngRandOff);
                Vector3 vOffset = Quaternion.AngleAxis(m_fAngOff, Vector3.forward) * (AngRandRadious * Vector3.right);
                vTarget += vOffset;
                m_moving.Arrive(vTarget);
            }
            else // Follow the player
            {                
                // stop following the path when closed enough to target
                // 타겟과의 위치가 너무 가까워서 패스파인딩이 꺼질때만 손수 처리를 해주네 
                m_pathFindingBehaviour.enabled = (vTarget - transform.position).magnitude > MinDistToReachTarget;
                if (!m_pathFindingBehaviour.enabled)
                {
                    m_fAngOff += Random.Range(-AngRandOff, AngRandOff);
                    Vector3 vOffset = Quaternion.AngleAxis(m_fAngOff, Vector3.forward) * (AngRandRadious * Vector3.right);
                    vTarget += vOffset;
                    Debug.DrawLine(transform.position, m_player.transform.position, Color.blue);
                    Debug.DrawRay(m_player.transform.position, vOffset, Color.blue);

                    m_moving.Arrive(vTarget);
                }
            }

            //+++avoid obstacles
            // 충돌이 걸리면 현재 속도의 방향을 반대로 바꿔버리네 
            // 급제동을 건다고 이해하면 될라나
            Vector3 vTurnVel = Vector3.zero;
			if ( 0 != (m_phyChar.CollFlags & PhysicCharBehaviour.eCollFlags.RIGHT))
			{
                vTurnVel.x = -m_moving.MaxSpeed;
			}
			else if ( 0 != (m_phyChar.CollFlags & PhysicCharBehaviour.eCollFlags.LEFT))
			{
                vTurnVel.x = m_moving.MaxSpeed;
			}
			if ( 0 != (m_phyChar.CollFlags & PhysicCharBehaviour.eCollFlags.DOWN))
			{
                vTurnVel.y = m_moving.MaxSpeed;
			}
			else if ( 0 != (m_phyChar.CollFlags & PhysicCharBehaviour.eCollFlags.UP))
			{
                vTurnVel.y = -m_moving.MaxSpeed;
			}
            if( vTurnVel != Vector3.zero )
            {
                m_moving.ApplyForce(vTurnVel - m_moving.Veloc);
            }
            //---

            //fix to avoid flickering of the creature when collides with wall
            if (Time.frameCount % 16 == 0)
            //---            
            {
                if (!LockAnimDir) UpdateAnimDir();
            }
		}
	}
}