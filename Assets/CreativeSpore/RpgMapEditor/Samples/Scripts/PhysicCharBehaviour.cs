using UnityEngine;
using System.Collections;
using CreativeSpore.RpgMapEditor;

namespace CreativeSpore
{
	public class PhysicCharBehaviour : MonoBehaviour {

		[System.Flags]
		public enum eCollFlags
		{
			NONE = 0,
			DOWN = (1 << 0),
			LEFT = (1 << 1),
			RIGHT = (1 << 2),
			UP = (1 << 3)
		}

		public Vector3 Dir;
		public float MaxSpeed = 1f;
        public bool IsCollEnabled = true;

		private Vector3 m_vPrevPos;
		private float m_speed;

		public eCollFlags CollFlags = eCollFlags.NONE;

        // 충돌 렉트
        // width는 0.14, 0.14 -와 +의 균형이 같다, height는 0.04, 0.08로 +가 좀 더 길다 
        public Rect CollRect = new Rect(-0.14f, -0.04f, 0.28f, 0.12f);

		public bool IsMoving
		{
			get { return Dir.sqrMagnitude > 0; }
		}

        void Start()
        {
            m_vPrevPos = transform.position;
        }

		// Update is called once per frame
		void Update () 
		{
			RpgMapHelper.DebugDrawRect( transform.position, CollRect, Color.white );

            // 벡터 길이의 제곱한 값 > 0 이라는 것은 방향 벡터가 있다는 얘기겠지 
            if (Dir.sqrMagnitude > 0f)
			{
                // divide by n per second ( n:2 )
                // 그래서 속도를 늘리는거고
                // MaxSpeed - m_speed 이 계산자체가 현재 스피드가 최대 스피드를 넘어서면 -가 되버리기 때문에 속도에 제한을 걸 수 있게 된다 
                m_speed += (MaxSpeed - m_speed) / Mathf.Pow(2f, Time.deltaTime);
			} 
			else
			{
                // 방향벡터가 0보다 작거나 같다는건 방향이 정해져있지 않다는 얘기
                // 속도를 지속적으로 낮춘다 
				m_speed /= Mathf.Pow(2f, Time.deltaTime);
			}
			Dir.z = 0f;
			transform.position += Dir * m_speed * Time.deltaTime;

            if (IsCollEnabled)
            {
                DoCollisions();
            }
		}

		const int k_subDiv = 6; // sub divisions
		public bool IsColliding( Vector3 vPos )
		{
            // 이 충돌체크 로직은 부하의 소지가 다분히 있다 
            // 예를 들어 CollRect가 20,20이라는 width, height를 가지고 있다면 x ~ x+20을 6번 쪼개서 일일히 충돌됬는지 안됬는지 체크한다 
            // y축도 예외없다. 만약 충돌이 일어나지 않는다면 36번의 충돌체크를 한다는 얘기 
			Vector3 vCheckedPos = Vector3.zero;
			for (int i = 0; i < k_subDiv; ++i)
			{
				for( int j = 0; j < k_subDiv; ++j)
				{
					vCheckedPos.x = vPos.x + Mathf.Lerp( CollRect.x, CollRect.x + CollRect.width, (float)i / (k_subDiv-1));
					vCheckedPos.y = vPos.y + Mathf.Lerp( CollRect.y, CollRect.y + CollRect.height, (float)j / (k_subDiv-1));

					eTileCollisionType collType = AutoTileMap.Instance.GetAutotileCollisionAtPosition( vCheckedPos );
					if( collType != eTileCollisionType.PASSABLE && collType != eTileCollisionType.OVERLAY )
					{
						return true;
					}
				}
			}
			return false;
		}

		void DoCollisions() 
		{
			Vector3 vTempPos = transform.position;
			Vector3 vCheckedPos = transform.position;
			CollFlags = eCollFlags.NONE;

            // 현재 위치가 충돌에 걸렸다 
			if( IsColliding( vCheckedPos ) )
			{
				//m_speed = 0f;
				vCheckedPos.y = m_vPrevPos.y;

                // 근데 예전 위치에서의 y축 충돌이 아니었다면
				if( !IsColliding( vCheckedPos ) )
				{
                    // 현재 위치에서의 y축 충돌이기 때문에 충돌 플래그는 Up/Down
					vTempPos.y = m_vPrevPos.y;
					CollFlags |= m_vPrevPos.y > transform.position.y? eCollFlags.DOWN : eCollFlags.UP;
				}
				else
				{
                    // 예전 위치에서도 y축 충돌이 되어있었다면 

					vCheckedPos = transform.position;
					vCheckedPos.x = m_vPrevPos.x;

                    // 예전 위치에서의 x축 충돌이 안 일어났다면
					if( !IsColliding( vCheckedPos ) )
					{
                        // 현재 위치에서의 x축 충돌이기 때문에 Left/Right 결정 
						vTempPos.x = m_vPrevPos.x;
						CollFlags |= m_vPrevPos.x > transform.position.x? eCollFlags.LEFT : eCollFlags.RIGHT;
					}
					else
					{
                        // 예전 위치에서도 x,y 축 충돌 모두 일어났었다면 
                        // 현재 위치도 현상 유지라는 소리니 모든 방향에 Up/Down, Left/Right 결정 
						vTempPos = m_vPrevPos;
						CollFlags |= m_vPrevPos.y > transform.position.y? eCollFlags.DOWN : eCollFlags.UP;
						CollFlags |= m_vPrevPos.x > transform.position.x? eCollFlags.LEFT : eCollFlags.RIGHT;
					}
				}
				transform.position = vTempPos;
			}
			else
			{
				//image_blend = c_white;
			}

            // 충돌이 걸렸다면 이전 위치로 회귀되어 다음 위치로 이동할 수 없게 막고
            // 충돌이 걸리지 않았다면 회귀되지 않고 이동이 진행되겠지 
			transform.position = vTempPos;
			m_vPrevPos = transform.position;
		}
	}
}
