﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;

namespace CreativeSpore.RpgMapEditor
{
    /// <summary>
    /// Map data containing all tiles and the size of the map
    /// </summary>
	[System.Serializable, XmlRoot("AutoTileMap")]    
	public class AutoTileMapSerializeData 
	{
        public const string k_version = "1.2.4";//TODO: change this after each update!

		[System.Serializable]
		public class MetadataChunk
		{
            public string version = k_version; 
			public bool compressedTileData = true;

            public bool IsVersionAboveOrEqual(string versionToCompare)
            {
                string[] curVersion = version.Split('.');
                string[] compareVersion = versionToCompare.Split('.');
                for( int i = 0; i < curVersion.Length && i < compareVersion.Length; ++i )
                {
                    if (System.Convert.ToInt32(curVersion[i]) < System.Convert.ToInt32(compareVersion[i]))
                        return false;
                }
                return compareVersion.Length >= curVersion.Length;
            }
		}

		[System.Serializable]
		public class TileLayer
		{
			public List<int> Tiles;
            public bool Visible = true;
            public string Name;
            public eLayerType LayerType;
            public string SortingLayer = "Default"; // sorting layer
            public int SortingOrder = 0; // sorting order
            public float Depth;
		}

		public MetadataChunk Metadata = new MetadataChunk(); 
		public int TileMapWidth;
		public int TileMapHeight;
        public List<TileLayer> TileData = new List<TileLayer>();

	 	public void CopyData (AutoTileMapSerializeData mapData)
		{
			Metadata = mapData.Metadata;
			TileMapWidth = mapData.TileMapWidth;
			TileMapHeight = mapData.TileMapHeight;
			TileData = mapData.TileData;
		}

        /// <summary>
        /// Save the map configuration
        /// </summary>
        /// <param name="_autoTileMap"></param>
        /// <returns></returns>
		public bool SaveData( AutoTileMap _autoTileMap, int width = -1, int height = -1 )
		{
            if (width < 0) width = TileMapWidth;
            if (height < 0) height = TileMapHeight;
            // avoid clear map data when auto tile map is not initialized
			if( !_autoTileMap.IsInitialized )
			{
				//Debug.LogError(" Error saving data. Autotilemap is not initialized! Map will not be saved. ");
				return false;
			}

            Metadata.version = k_version;
			
			TileData.Clear();
			for( int iLayer = 0; iLayer < _autoTileMap.GetLayerCount(); ++iLayer )
			{
                AutoTileMap.MapLayer mapLayer = _autoTileMap.MapLayers[iLayer];
                List<int> tileData = new List<int>(width * height);
				int iTileRepetition = 0;
				int savedTileId = 0;

                int mapWidth = _autoTileMap.MapTileWidth;
                int mapHeight = _autoTileMap.MapTileHeight;
                for (int tile_y = 0; tile_y < height; ++tile_y)
				{
                    for (int tile_x = 0; tile_x < width; ++tile_x)
					{
                        int iType = -1;

                        // 이미 위 for문에서 < width, < height를 해놓고 다시 if를 쓰는 이유가 뭐야... 뭐 강조하려고 적어논거야 뭐야
                        if (tile_x < mapWidth && tile_y < mapHeight)
                        {
                            AutoTile autoTile = _autoTileMap.TileLayers[_autoTileMap.MapLayers[iLayer].TileLayerIdx][tile_x + tile_y * mapWidth];
                            iType = autoTile != null? autoTile.Id : -1;
                        }

                        //+++fix: FogOfWar tiles could be < -1, and this is not good for compress system, excepting ids >= -1
                        if (mapLayer.LayerType == eLayerType.FogOfWar)
                        {
                            iType = ((iType >> 1) & 0x7FFFFFFF); // remove the last bit of the last byte. Will be << 1 later when loading
                        }
                        //---

						if( iTileRepetition == 0 )
						{
							savedTileId = iType;
							iTileRepetition = 1;
						}
						else
						{
							// compression data. All tiles of the same type are store with number of repetitions ( negative number ) and type
							// ex: 5|5|5|5 --> |-4|5| (4 times 5) ex: -1|-1|-1 --> |-3|-1| ( 3 times -1 )
							if( iType == savedTileId ) ++iTileRepetition;
							else
							{
								if( iTileRepetition > 1 )
								{
									tileData.Add( -iTileRepetition ); // save number of repetition with negative sign
								}
                                if( savedTileId < -1 )
                                {
                                    Debug.LogError(" Wrong tile id found when compressing the tile layer " + mapLayer.Name);
                                    savedTileId = -1;
                                }
								tileData.Add( savedTileId );
								savedTileId = iType;
								iTileRepetition = 1;
							}
						}
					}
				}
				// save last tile type found
				if( iTileRepetition > 1 )
				{
					tileData.Add( -iTileRepetition );
				}
				tileData.Add( savedTileId );

				// 
                TileData.Add(new TileLayer() 
                { 
                    Tiles = tileData, 
                    Depth = mapLayer.Depth, 
                    LayerType = mapLayer.LayerType, 
                    SortingLayer = mapLayer.SortingLayer,
                    SortingOrder = mapLayer.SortingOrder,
                    Name = mapLayer.Name, 
                    Visible = mapLayer.Visible 
                });
			}
            TileMapWidth = width;
            TileMapHeight = height;
			return true;
		}

        /// <summary>
        /// Get this object serialized as an xml string
        /// </summary>
        /// <returns></returns>
		public string GetXmlString()
		{
			return UtilsSerialize.Serialize<AutoTileMapSerializeData>(this);
		}

        /// <summary>
        /// Save this object serialized in an xml file
        /// </summary>
        /// <param name="_filePath"></param>
		public void SaveToFile(string _filePath)
		{
			var serializer = new XmlSerializer(typeof(AutoTileMapSerializeData));
			var stream = new FileStream(_filePath, FileMode.Create);
			serializer.Serialize(stream, this);
			stream.Close();
		}

        /// <summary>
        /// Create map serialized data from xml file
        /// </summary>
        /// <param name="_filePath"></param>
        /// <returns></returns>
		public static AutoTileMapSerializeData LoadFromFile(string _filePath)
		{
			var serializer = new XmlSerializer(typeof(AutoTileMapSerializeData));
			var stream = new FileStream(_filePath, FileMode.Open);
			var obj = serializer.Deserialize(stream) as AutoTileMapSerializeData;
			stream.Close();
			return obj;
		}

        /// <summary>
        /// Create map serialized data from xml string
        /// </summary>
        /// <param name="_xml"></param>
        /// <returns></returns>
		public static AutoTileMapSerializeData LoadFromXmlString(string _xml)
		{
			return UtilsSerialize.Deserialize<AutoTileMapSerializeData>(_xml);
		}

        /// <summary>
        /// Load map serialized data into a map
        /// </summary>
        /// <param name="_autoTileMap"></param>
        public IEnumerator LoadToMap(AutoTileMap _autoTileMap)
		{
			_autoTileMap.Initialize();
			_autoTileMap.ClearMap();
            
            if( !Metadata.IsVersionAboveOrEqual("1.2.4") )
            {
                _applyFixVersionBelow124(_autoTileMap);
            }

            int totalMapTiles = TileMapWidth * TileMapHeight;
			for( int iLayer = 0; iLayer < TileData.Count; ++iLayer )
			{
                TileLayer tileData = TileData[iLayer];

                // 해당 레이어 정보만 따로 모아둔 리스트 
                _autoTileMap.MapLayers.Add(             
                    new AutoTileMap.MapLayer() 
                    {
                        Name = tileData.Name,
                        Visible = tileData.Visible, 
                        LayerType = tileData.LayerType, 
                        SortingOrder = tileData.SortingOrder,
                        SortingLayer = tileData.SortingLayer,
                        Depth = tileData.Depth,
                        TileLayerIdx = iLayer,
                    });

                // 이게 진짜 레이어별로 타일들을 담아둔 리스트
                // 논리대로라면 이것도 MapLayers안에 포함되어있어야 하는게 맞다
                // 맵 데이타에 그런식으로 작성되어있으니까 

                // widht, height 만큼 만드네 1픽셀당 하나의 타일을 둔다는 의미인건가. 
                // 그게 아니라 하나의 타일을 만든다면 여러개의 픽셀에 참조로 들어간다는 의미겠지?
                _autoTileMap.TileLayers.Add( new AutoTile[TileMapWidth * TileMapHeight] );      
                int iTileRepetition = 1;
				int iTileIdx = 0;
                for (int i = 0; i < tileData.Tiles.Count; ++i )
                {
                    // 이 타입이라는 값이 맵 데이타의 각 타일의 적혀있는 의문의 숫자를 뜻함
                    int iType = tileData.Tiles[i];
                    //see compression notes in CreateFromTilemap
                    if (iType < -1)
                    {
                        // 현재 타일데이타 상의 숫자가 -1보다 작은 수라면 
                        // 그 -값을 +로 바꿔서 iTileRepetition 변수에 저장해둠
                        // 그리고 다음 타일로 넘어감
                        // 분명 다음 타일에 이값이 쓰이게되겠지 
                        iTileRepetition = -iType;
                    }
                    else
                    {
                        //+++fix: FogOfWar tiles could be < -1, and this is not good for compress system, excepting ids >= -1
                        if (tileData.LayerType == eLayerType.FogOfWar)
                        {
                            iType = (iType << 1); //restore value so the lost bit was the less significant of last byte
                        }
                        // 총 타일 수보다 iTileRepetition값이 크면 압축 에러로 간주하고 무효값으로 만드네 
                        if (iTileRepetition > totalMapTiles)
                        {
                            Debug.LogError("Error uncompressing layer " + tileData.Name + ". The repetition of a tile was higher than map tiles " + iTileRepetition + " > " + totalMapTiles);
                            iTileRepetition = 0;
                        }

                        // 이 for문을 통해 알 수 있는 사실은 iTileRepetition의 값이 굉장히 중요하다는 것
                        // iTileRepetition의 기본값은 1
                        // 데이타 상의 음수값은 어떤 타일 타입의 '갯수'를 의미함
                        // 허나 이건 전체 타일맵 안에서의 실제 갯수를 의미하는 건 아니고, 연속으로 이어지는 타일 갯수를 의미한다고 보면 됨
                        for (; iTileRepetition > 0; --iTileRepetition, ++iTileIdx)
                        {
                            // 맵이 커서 로딩 시간이 길어질 수 있으니 중간중간 멈추는 식별자를 줘서 로딩바를 갱신하는 시간을 주는 개념이군 
                            // 하지만 현재 코드는 외부에서 MoveNext()를 while로 끝날때까지 돌리기 때문에 이 코드는 의미가 없다 
                            // 여기서 빠져나와봤자 다음 틱때 바로 들어온다 
                            if (iTileIdx % 10000 == 0)
                            {
                                //float loadingPercent = ((float)(iTileIdx + iLayer * TileMapWidth * TileMapHeight)) / (TileMapWidth * TileMapHeight * TileData.Count);
                                //Debug.Log(" Loading " + (int)(loadingPercent * 100) + "%");
                                yield return null;
                            }

                            // 아래의 계산을 통해 알게된 타일 생성 순서 및 타일 인덱싱 순서. 200 * 200이라 가정했을 때 
                            // 0   ~ 199
                            // 199 ~ 399 
                            // ... 
                            // 실제 타일 데이터를 이용해서 유니티 씬 좌표를 어떻게 찍을지는 좀 더 지켜봐야알 듯 
                            int tile_x = iTileIdx % TileMapWidth;
                            int tile_y = iTileIdx / TileMapHeight;
                            if (iType >= 0 || tileData.LayerType == eLayerType.FogOfWar)
                            {
                                _autoTileMap.SetAutoTile(tile_x, tile_y, iType, iLayer, false);
                            }
                        }
                        iTileRepetition = 1;
                    }
                }
			}

            // 별거 아닌 듯이 넘어갔던 이 코드들이 데이타를 통해서 씬에 타일을 만드는 역할을 하는 코드들이구마잉 
            // 감싸고 있는 함수가 LoadToMap이면 Load만 하라고 Create도 같이 하지 말구연;;

            // 이 함수는 어쩌면 좀 야매라고도 할 수 있다
            // 타일을 갱신하려면 Refresh 스택에 타일을 넣어놔야 하는데 그 작업을 하기 위해서 수백 수천개의 타일을 리스트에 때려박는 역할을 한다 
            // 부하가 엄청 걸리는 작업이 될 수 있음
            _autoTileMap.RefreshAllTiles();

            // 위에서 갱신하는 것을 가지고 뭔가 만드는 작업을 할 것 같은 함수라 생각했지만 그게 아님
            // 위 함수에서 청크 생성까지 다 해버리기 때문에 이 함수는 하는 일이 거의 없음 
            _autoTileMap.UpdateChunkLayersData();


			_autoTileMap.RefreshMinimapTexture();
		}

        private void _applyFixVersionBelow124(AutoTileMap _autoTileMap)
        {
            for (int iLayer = 0; iLayer < TileData.Count; ++iLayer)
            {
                TileLayer tileData = TileData[iLayer];
                tileData.Visible = true;
                switch (iLayer)
                {
                    case 0:
                        tileData.Name = "Ground";
                        tileData.LayerType = eLayerType.Ground;
                        tileData.Depth = 1f;
                        break;
                    case 1:
                        tileData.Name = "Ground Overlay";
                        tileData.LayerType = eLayerType.Ground;
                        tileData.Depth = 0.5f;
                        break;
                    case 2:
                        tileData.Name = "Overlay";
                        tileData.LayerType = eLayerType.Overlay;
                        tileData.Depth = -1f;
                        break;
                }
            }
        }
	}
}
