﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CreativeSpore.RpgMapEditor
{

    /// <summary>
    /// Manages the creation of all map tile chunks
    /// </summary>
	public class TileChunkPool : MonoBehaviour 
	{
        /// <summary>
        /// The width size of the generated tilechunks in tiles ( due max vertex limitation, this should be less than 62 )
        /// Increasing this value, map will load faster but drawing tiles will be slower
        /// </summary>
		public const int k_TileChunkWidth = 62;

        /// <summary>
        /// The height size of the generated tilechunks in tiles ( due max vertex limitation, this should be less than 62 )
        /// Increasing this value, map will load faster but drawing tiles will be slower
        /// </summary>
		public const int k_TileChunkHeight = 62;

		[System.Serializable]
		public class TileChunkLayer
		{
			public GameObject ObjNode;
			public TileChunk[] TileChunks;
            public int SortingOrder 
            { 
                get{ return _sortingOrder; }
                set
                {
                    _sortingOrder = value;
                    for (int i = 0; i < TileChunks.Length; ++i )
                    {
                        if (TileChunks[i] != null) TileChunks[i].SortingOrder = value;
                    }
                } 
            }

            public string SortingLayer
            {
                get { return _sortingLayer; }
                set
                {
                    _sortingLayer = value;
                    for (int i = 0; i < TileChunks.Length; ++i)
                    {
                        if (TileChunks[i] != null) TileChunks[i].SortingLayer = value;
                    }
                }
            }

            private int _sortingOrder = 0;
            private string _sortingLayer = "Default";
        }

		public List<TileChunkLayer> TileChunkLayers = new List<TileChunkLayer>();

		private List<TileChunk> m_tileChunkToBeUpdated = new List<TileChunk>();
		[SerializeField]
		private AutoTileMap m_autoTileMap;

		public void Initialize (AutoTileMap autoTileMap)
		{
			hideFlags = HideFlags.NotEditable;
			m_autoTileMap = autoTileMap;
			foreach( TileChunkLayer tileChunkLayer in TileChunkLayers )
			{
				if( tileChunkLayer.ObjNode != null )
				{
				#if UNITY_EDITOR
					DestroyImmediate(tileChunkLayer.ObjNode);
				#else
					Destroy(tileChunkLayer.ObjNode);
				#endif
				}
			}
			TileChunkLayers.Clear();
			m_tileChunkToBeUpdated.Clear();
		}

        /// <summary>
        /// Mark a tile to be updated during update
        /// </summary>
        /// <param name="tileX"></param>
        /// <param name="tileY"></param>
        /// <param name="layer"></param>
		public void MarkUpdatedTile( int tileX, int tileY, int layer )
		{
			TileChunk tileChunk = _GetTileChunk( tileX, tileY, layer );
			if( !m_tileChunkToBeUpdated.Contains(tileChunk) )
			{
				m_tileChunkToBeUpdated.Add( tileChunk );
			}
		}

        /// <summary>
        /// Mark all tilechunks of a layer to be updated
        /// </summary>
        /// <param name="layer"></param>
        public void MarkLayerChunksForUpdate( int layer )
        {
            TileChunkLayer chunkLayer = _GetTileChunkLayer(layer);
            m_tileChunkToBeUpdated.AddRange(chunkLayer.TileChunks);
        }

        /// <summary>
        /// Update marked chunks
        /// </summary>
		public void UpdateChunks()
		{
            IEnumerator coroutine = UpdateChunksAsync();
            while (coroutine.MoveNext());
		}

        /// <summary>
        /// Update marked chunks asynchronously
        /// </summary>
        public IEnumerator UpdateChunksAsync()
        {
            // 후입선출의 형태를 띄고 있는 스택 구조를 가지고 있구먼
            // 갯수가 0이 될때까지 첫번째꺼를 Refresh 한 다음 목록에서 빼버리고를 반복한다 
            while (m_tileChunkToBeUpdated.Count > 0)
            {
                m_tileChunkToBeUpdated[0].RefreshTileData();
                m_tileChunkToBeUpdated.RemoveAt(0);
                yield return null;
            }
        }

		public void UpdateLayersData ( )
		{            
			for( int i = 0; i < TileChunkLayers.Count; ++i )
			{
                AutoTileMap.MapLayer mapLayer = m_autoTileMap.MapLayers[i];
                TileChunkLayer tileChunkLayer = TileChunkLayers[i];
                tileChunkLayer.ObjNode.SetActive(mapLayer.Visible);
                tileChunkLayer.ObjNode.transform.localPosition = Vector3.zero + new Vector3(0f, 0f, mapLayer.Depth);
                tileChunkLayer.SortingLayer = mapLayer.SortingLayer;
                tileChunkLayer.SortingOrder = mapLayer.SortingOrder;               
			}
		}

        // TODO: When a layer is created with no tiles ( all set to -1 ) layer is never created and this is causing problems
        // but I should find a better way of doing this. TileChunkPool should be redesigned
        public void CreateLayers( int totalLayers )
        {
            _CreateTileChunkLayer(totalLayers - 1);
        }

		private TileChunk _GetTileChunk( int tileX, int tileY, int layer )
		{
			TileChunkLayer chunkLayer = _GetTileChunkLayer( layer );

            // 일단 타일청크를 여러개로 두는 이유는 아직 모르겠다
            // 허나 x,y 위치만으로 타일청크의 인덱스를 검색하는 계산식은 내공이 느껴진다 
            // 일단 이거는 width 200기준 4라는 고정값이 나옴
            // 타일 청크를 여러개 둔 이유는 최적화 때문이 아닐까? 
            // 잘게잘게 쪼개면 쪼갤수록 청크가 책임져야 하는 애니메이션 타일의 수가 적어질테니 
            // 그걸 노리고 한 것일수도 있곘다 
			int rowTotalChunks = 1 + ((m_autoTileMap.MapTileWidth - 1) / k_TileChunkWidth);

            // 이 청크 인덱스 계산을 알아야하는 이유는 이걸 알아야 
            // 어떤 픽셀 좌표에 있는게 어느 청크에 렌더링 되고 있는지 알 수 있기 때문임
            // 아래 계산식을 참조로 청크 인덱싱을 알아낼 수 있음  
            // 0,   0   = 0                   x : 0   ~ 61,    y : 0   ~ 61
            // 62,  0   = 1                   x : 62  ~ 123,   y : 0   ~ 61
            // 124, 0   = 2                   x : 124 ~ 185,   y : 0   ~ 61
            // 186. 0   = 3                   x : 186 ~ 200,   y : 0   ~ 61
            // 0,   62  = 4                   x : 0   ~ 61,    y : 62  ~ 123
            // 62,  62  = 5                   x : 62  ~ 123,   y : 62  ~ 123
            // 124, 62  = 6                   x : 124 ~ 185,   y : 62  ~ 123
            // 186, 62  = 7                   x : 186 ~ 200,   y : 62  ~ 123
            // 0,   124 = 8                   x : 0   ~ 61,    y : 124 ~ 185
            // 62,  124 = 9                   x : 62  ~ 123,   y : 124 ~ 185
            // 124, 124 = 10                  x : 124 ~ 185,   y : 124 ~ 185
            // 186, 124 = 11                  x : 186 ~ 200,   y : 124 ~ 185
            // 0,   186 = 12                  x : 0   ~ 61,    y : 186 ~ 200
            // 62,  186 = 13                  x : 62  ~ 123,   y : 186 ~ 200
            // 124, 186 = 14                  x : 124 ~ 185,   y : 186 ~ 200
            // 186, 186 = 15                  x : 186 ~ 200,   y : 186 ~ 200
            // 위 범위표를 통해 알게되는건 타일의 0,0은 좌상단 끝이고 200,200은 후하단끝이다 
            int chunkIdx = (tileY / k_TileChunkHeight) * rowTotalChunks + (tileX / k_TileChunkWidth);

            // 내가 좀 헷갈린게 있다면 chunkLayer.TileChunks를 할당할 때 무조건 16개씩 할당을 했단 말이지
            // 그래서 그 안에 가지고 있던것도 16개 전부 들어가 있을꺼라고 생각을 했었는데 그게 아니었던 것 같다 
            // 담아야하는 방을 할당 하는 것과 내용물을 넣는 것은 다른 개념인 듯
            // 유니티 씬에서 보면 그라운드는 16개가 생성이 되어있는데, 다른 레이어는 16개가 안 차있다 
            // 그 이유는 그라운드는 모든 위치에 꽉차게 포진되어있지만, 다른 레이어는 꽉차게 쓰이기 않고있기 때문
            // 청크는 진짜 사용되는 타일이 있을 경우에만 생성이 된다 
            TileChunk tileChunk = chunkLayer.TileChunks[chunkIdx];
			if( tileChunk == null )
			{
				int startTileX = tileX - tileX % k_TileChunkWidth;
				int startTileY = tileY - tileY % k_TileChunkHeight;
				GameObject chunkObj = new GameObject();
                chunkObj.name = m_autoTileMap.MapLayers[layer].Name +"_" + startTileX + "_" + startTileY;
				chunkObj.transform.SetParent( chunkLayer.ObjNode.transform, false);
                chunkObj.layer = chunkLayer.ObjNode.layer;
				//chunkObj.hideFlags = HideFlags.NotEditable;
				tileChunk = chunkObj.AddComponent<TileChunk>();
				chunkLayer.TileChunks[chunkIdx] = tileChunk;
				tileChunk.Configure( m_autoTileMap, layer, startTileX, startTileY, k_TileChunkWidth, k_TileChunkHeight );
			}
			return tileChunk;
		}

		private TileChunkLayer _GetTileChunkLayer( int layer )
		{
			return TileChunkLayers.Count > layer? TileChunkLayers[layer] : _CreateTileChunkLayer( layer );
		}

		private TileChunkLayer _CreateTileChunkLayer( int layer )
		{
            // 계산상으로보면 맵 타일 200*200 기준. 4,4가 나옴 
            // totalChunks값은 16이 나오게 됨
            int rowTotalChunks = 1 + ((m_autoTileMap.MapTileWidth - 1) / k_TileChunkWidth);
			int colTotalChunks = 1 + ((m_autoTileMap.MapTileHeight - 1) / k_TileChunkHeight);
			int totalChunks = rowTotalChunks * colTotalChunks;
			TileChunkLayer chunkLayer = null;

            // 만약 타일 청크 레이어안의 모든 레이어가 다 박혀있다면 무조건 레이어 enum 수보다 +1이 된다
            // 타일 청크 레이어의 타일이 꽉 차 있다면 null이 반환된다는 얘기 
            // 이 함수가 좀 마음에 안 드는게 있다면 이 함수의 목적은 타일 청크 레이어를 만들고 그것을 반환하는 것임
            // 만약 이미 만들어서 존재한다면 만들어져 있는걸 반환하는 게 더 좋을 것 같다 null을 뱉지 말고
			while( TileChunkLayers.Count <= layer )
			{
				chunkLayer = new TileChunkLayer();
				chunkLayer.TileChunks = new TileChunk[ totalChunks ];
				chunkLayer.ObjNode = new GameObject();
				chunkLayer.ObjNode.transform.parent = transform;
                chunkLayer.ObjNode.transform.localPosition = Vector3.zero + new Vector3(0f, 0f, m_autoTileMap.MapLayers[TileChunkLayers.Count].Depth);
                chunkLayer.ObjNode.transform.localRotation = Quaternion.identity;
                chunkLayer.SortingOrder = m_autoTileMap.MapLayers[TileChunkLayers.Count].SortingOrder;
                chunkLayer.SortingLayer = m_autoTileMap.MapLayers[TileChunkLayers.Count].SortingLayer;
                chunkLayer.ObjNode.name = m_autoTileMap.MapLayers[TileChunkLayers.Count].Name + "_" + TileChunkLayers.Count;
				TileChunkLayers.Add( chunkLayer );
			}
			return chunkLayer;
		}
	}
}