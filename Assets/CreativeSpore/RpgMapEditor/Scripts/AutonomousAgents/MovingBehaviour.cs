﻿using UnityEngine;
using System.Collections;
using CreativeSpore.RpgMapEditor;
using System.Collections.Generic;
using CreativeSpore.PathFindingLib;

namespace CreativeSpore
{
    //ref: http://natureofcode.com/book/chapter-6-autonomous-agents/
	public class MovingBehaviour : MonoBehaviour {

		public Vector3 Veloc;
		public Vector3 Acc;
		public float Radious = 0.16f;
		public float MaxForce = 0.01f;
		public float MaxSpeed = 0.01f;

		// Use this for initialization
		void Start () 
		{
			
		}

		static void LimitVect( ref Vector3 vRef, float limit )
		{
			if (vRef.sqrMagnitude > limit * limit)
			{
				vRef = vRef.normalized * limit;
			}
		}
		
		// Update is called once per frame
		void Update () 
		{
			Veloc += Acc;
			Veloc.z = 0f;
			LimitVect( ref Veloc, MaxSpeed );
			transform.position += Veloc * Time.deltaTime;
			Acc = Vector3.zero;
		}

		public void ApplyForce( Vector3 vForce )
		{
			Acc += vForce;
		}

		public void Seek( Vector3 vTarget )
		{
            // 방햑벡터 구하기
			Vector3 vDir = vTarget - transform.position;
	//		float dist = vDir.magnitude;
			vDir.Normalize();

            // 방향벡터에다가 최고스피드를 곱해버리네
			vDir *= MaxSpeed;

            // 곱한거에 현재 속도를 뺀다라...
            // 근데 웃긴거 현재 속도를 뺀 방향벡터 크기가 ApplyForce에 의해 다음 틱 Update 때 현재 속도에 더해짐
			Vector3 vSteer = vDir - Veloc;
			LimitVect( ref vSteer, MaxForce );
			ApplyForce( vSteer );
		}

		public void Arrive( Vector3 vTarget )
		{
			Vector3 vDir = vTarget - transform.position;
			vDir.Normalize();

            // 위에 seek 코드와 다른게 있다면 이 코드뿐
            float dist = vDir.magnitude;
            if (dist <= Radious)
			{
                // Radious값은 0.16 고정 
                // dist가 vDir.magnitude라 해도 그 위에 코드에서 vDir을 normalize했기 때문에 dist는 1 이하다 

                // dist가 radious보다 작아야할 때라는 말은 거의 초근접상황이라는 말 

                // Mathf.Lerp의 t를 계산이 dist/Radious는 무조건 0~1사이의 값이기 때문에 a,b 매개변수의 0,1은 아예 의미가 없는 매개변수다 
                // vDir *= dist/Radious; 와 똑같다

                // 이 코드가 존재하는 이유는 초근접상황일 때 타겟위치보다 넘어가지 않도록 브레이크를 밟아 속도를 줄이는 것임
                vDir *= Mathf.Lerp(0f, 1f, dist/Radious);
			} 

			vDir *= MaxSpeed;
			Vector3 vSteer = vDir - Veloc;
			LimitVect( ref vSteer, MaxForce );
			ApplyForce( vSteer );
		}

        // This function implements Craig Reynolds' path following algorithm
        // http://www.red3d.com/cwr/steer/PathFollow.html
        /// <summary>
        /// Follow a path of positions
        /// </summary>
        /// <param name="pathList"></param>
        /// <param name="pathRadius"></param>
        public void Follow(List<Vector3> pathList, float pathRadius)
        {

            if( pathList.Count <= 1 )
            {
                return;
            }

            // Predict location 50 (arbitrary choice) frames ahead
            // This could be based on speed 
            Vector3 predict = Veloc;
            predict.Normalize();
            predict *= 0.3f;
            Vector3 predictLoc = transform.position + predict;
            Debug.DrawLine(transform.position, predictLoc, Color.blue);

            // Now we must find the normal to the path from the predicted location
            // We look at the normal for each line segment and pick out the closest one
            
            Vector3 target = Vector3.zero;
            float worldRecord = float.MaxValue;  // Start with a very high record distance that can easily be beaten

            // Loop through all points of the path
            for (int i = 0; i < pathList.Count - 1; i++)
            {

                // Look at a line segment
                Vector3 a = pathList[i];
                Vector3 b = pathList[i + 1];

                // Get the normal point to that line
                Vector3 normalPoint = GetNormalPoint(predictLoc, a, b);

                if (normalPoint.x < a.x || normalPoint.x > b.x || normalPoint.y < a.y || normalPoint.y > b.y)
                {
                    // This is something of a hacky solution, but if it's not within the line segment
                    // consider the normal to just be the end of the line segment (point b)
                    normalPoint = b;
                }

                // How far away are we from the path?
                float distance = Vector3.Distance(predictLoc, normalPoint);
                // Did we beat the record and find the closest line segment?
                if (distance < worldRecord)
                {
                    // If so the target we want to steer towards is the normal
                    worldRecord = distance;

                    // Look at the direction of the line segment so we can seek a little bit ahead of the normal
                    Vector3 dir = b - a;
                    dir.Normalize();
                    // This is an oversimplification
                    // Should be based on distance to path & velocity
                    dir *= 0.1f;
                    target = normalPoint;
                    target += dir;

                    //Debug.DrawLine(predictLoc, a, Color.green);
                    //Debug.DrawLine(predictLoc, b, Color.cyan);
                    Debug.DrawLine(predictLoc, normalPoint, Color.white);
                }
            }

            // Only if the distance is greater than the path's radius do we bother to steer
            if (worldRecord > pathRadius)
            {
                Seek(target);
                Debug.DrawLine(transform.position, target, Color.magenta);
            }
            else
            {
                Debug.DrawLine(transform.position, target, Color.grey);
            }
        }

        // A function to get the normal point from a point (p) to a line segment (a-b)
        // This function could be optimized to make fewer new Vector objects
        Vector3 GetNormalPoint(Vector3 p, Vector3 a, Vector3 b)
        {
            // Vector from a to p
            Vector3 ap = p - a;
            // Vector from a to b
            Vector3 ab = b - a;
            ab.Normalize(); // Normalize the line
            // Project vector "diff" onto line by using the dot product
            ab *= Vector3.Dot(ap, ab);
            Vector3 normalPoint = a + ab;
            return normalPoint;
        }
	}
}
