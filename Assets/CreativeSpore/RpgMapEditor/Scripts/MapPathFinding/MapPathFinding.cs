﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CreativeSpore.PathFindingLib;

namespace CreativeSpore.RpgMapEditor
{

    public class MapTileNode : IPathNode
    {

        public int TileX { get; private set; }
        public int TileY { get; private set; }
        public int TileIdx { get; private set; }
        public Vector3 Position { get; private set; }
        public bool IsEmptyTilePassable = false; //TODO: Fix when this is true, is not working because is not taking into account when any of the layer has an empty tile
       
        List<int> m_neighborIdxList = new List<int>();
        MapPathFinding m_owner;

        internal MapTileNode(int idx, MapPathFinding owner) 
        {
            m_owner = owner;
            TileIdx = idx;
            TileX = idx % AutoTileMap.Instance.MapTileWidth;
            TileY = idx / AutoTileMap.Instance.MapTileHeight;
            Position = RpgMapHelper.GetTileCenterPosition(TileX, TileY);

            // get all neighbors row by row, neighIdx will be the idx of left most tile per each row
            int neighIdx = (idx-1)-AutoTileMap.Instance.MapTileWidth;
            for (int i = 0; i < 3; ++i, neighIdx += AutoTileMap.Instance.MapTileWidth)
            {
                for (int j = 0; j < 3; ++j)
                {
                    if (i != 1 || j != 1) // skip this node
                    {
                        m_neighborIdxList.Add(neighIdx + j);
                    }
                }
            }
        }

        #region IPathNode
        public override bool IsPassable() 
        {
            // 전체 타일맵 사이즈에 근거하여 타일의 위치가 타일맵안에 위치하는지 체크
            AutoTileMap autoTileMap = AutoTileMap.Instance;
            if (autoTileMap.IsValidAutoTilePos(TileX, TileY))
            {
                // 그라운드 레이어 찾기 (레이어하나 찾는데 for문 돌리는거 상당히 맘에 안듬)
                for( int iLayer = autoTileMap.GetLayerCount() - 1; iLayer >= 0; --iLayer )
                {
                    if( autoTileMap.MapLayers[iLayer].LayerType == eLayerType.Ground )
                    {
                        AutoTile autoTile = autoTileMap.GetAutoTile(TileX, TileY, iLayer);
                        eTileCollisionType collType = autoTile.Id >= 0 ? autoTileMap.Tileset.AutotileCollType[autoTile.Id] : eTileCollisionType.EMPTY;
                        if( IsEmptyTilePassable && collType == eTileCollisionType.EMPTY || 
                            collType == eTileCollisionType.PASSABLE || collType == eTileCollisionType.WALL )
                        {
                            return true;
                        }
                        else if( collType == eTileCollisionType.BLOCK || collType == eTileCollisionType.FENCE )
                        {
                            return false;
                        }
                    }
                }
            }
            return false;
        }

        public override float GetHeuristic( ) 
        {
            //NOTE: 10f in Manhattan and 14f in Diagonal should rally be 1f and 1.41421356237f, but I discovered by mistake these values improve the performance

            float h = 0f;

            switch( m_owner.HeuristicType )
            {
                case MapPathFinding.eHeuristicType.None: h = 0f; break;
                case MapPathFinding.eHeuristicType.Manhattan:
                    {
                        h = 10f * (Mathf.Abs(TileX - m_owner.EndNode.TileX) + Mathf.Abs(TileY - m_owner.EndNode.TileY));
                        break;
                    }
                case MapPathFinding.eHeuristicType.Diagonal:
                    {
                        float xf = Mathf.Abs(TileX - m_owner.EndNode.TileX);
                        float yf = Mathf.Abs(TileY - m_owner.EndNode.TileY);
                        if (xf > yf)
                            h = 14f * yf + 10f * (xf - yf);
                        else
                            h = 14f * xf + 10f * (yf - xf); 
                        break;
                    }
            }
            return h; 
        }

        // special case for walls
        bool _IsWallPassable( MapTileNode neighNode )
        {
            AutoTileMap autoTileMap = AutoTileMap.Instance;
            eTileCollisionType collType = eTileCollisionType.EMPTY;
            eTileCollisionType collTypeNeigh = eTileCollisionType.EMPTY;
            for (int iLayer = autoTileMap.GetLayerCount() - 1; iLayer >= 0; --iLayer)
            {
                if (autoTileMap.MapLayers[iLayer].LayerType == eLayerType.Ground)
                {
                    AutoTile autoTile = autoTileMap.GetAutoTile(TileX, TileY, iLayer);
                    AutoTile autoTileNeigh = autoTileMap.GetAutoTile(neighNode.TileX, neighNode.TileY, iLayer);

                    if (autoTile.Id == autoTileNeigh.Id) // you can walk over two wall tiles if they have the same type
                    {
                        if (autoTile.Id >= 0) 
                            return true;
                        else
                            continue;
                    }
                    else
                    {
                        // collType will keep the first collision type found of type wall or passable
                        if (collType != eTileCollisionType.PASSABLE && collType != eTileCollisionType.WALL)
                            collType = autoTile.Id >= 0 ? autoTileMap.Tileset.AutotileCollType[autoTile.Id] : eTileCollisionType.EMPTY;
                        if (collTypeNeigh != eTileCollisionType.PASSABLE && collTypeNeigh != eTileCollisionType.WALL)
                            collTypeNeigh = autoTileNeigh.Id >= 0 ? autoTileMap.Tileset.AutotileCollType[autoTileNeigh.Id] : eTileCollisionType.EMPTY;

                        if( collType == eTileCollisionType.PASSABLE && collTypeNeigh == eTileCollisionType.PASSABLE )
                        {
                            return true;
                        }
                        else if (collType == eTileCollisionType.WALL || collTypeNeigh == eTileCollisionType.WALL)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        public override float GetNeigborMovingCost(int neigborIdx) 
        {
            MapTileNode neighNode = GetNeighbor(neigborIdx) as MapTileNode;
            if (!_IsWallPassable(neighNode))
            {
                return PathFinding.k_InfiniteCostValue;
            }

            float fCost = 1f;
            //012 // 
            //3X4 // neighbor index positions, X is the position of this node
            //567
            // 친절하게 이웃 인덱스들의 설명을 적어놨군 
            
            // 대각선 이웃에 대해서만 처리를 해주려고 함 
            if( neigborIdx == 0 || neigborIdx == 2 || neigborIdx ==  5 || neigborIdx == 7 )
            {
                // 패스파인딩으로 대각선 이동을 허용하면 벽 타일에 부딪히는 경우가 잦고, 레트로 느낌이 살지가 않더라
                // 대각선 패스파인딩은 모조리 무시하기 위해 이렇게 해놈
                return PathFinding.k_InfiniteCostValue;

                //check if can reach diagonals as it could be not possible if flank tiles are not passable      
                MapTileNode nodeN = GetNeighbor(1) as MapTileNode;
                MapTileNode nodeW = GetNeighbor(3) as MapTileNode;
                MapTileNode nodeE = GetNeighbor(4) as MapTileNode;
                MapTileNode nodeS = GetNeighbor(6) as MapTileNode;
                if (
                    !m_owner.AllowDiagonals ||
                    (neigborIdx == 0 && (!nodeN.IsPassable() || !nodeW.IsPassable() || !_IsWallPassable(nodeN) || !_IsWallPassable(nodeW))) || // check North West
                    (neigborIdx == 2 && (!nodeN.IsPassable() || !nodeE.IsPassable() || !_IsWallPassable(nodeN) || !_IsWallPassable(nodeE))) || // check North East
                    (neigborIdx == 5 && (!nodeS.IsPassable() || !nodeW.IsPassable() || !_IsWallPassable(nodeS) || !_IsWallPassable(nodeW))) || // check South West
                    (neigborIdx == 7 && (!nodeS.IsPassable() || !nodeE.IsPassable() || !_IsWallPassable(nodeS) || !_IsWallPassable(nodeE)))    // check South East
                )
                {
                    // 대각선 노드의 양옆 노드가 통과될 수 없는 노드라면 가장 큰 비용이라 판단하고 바로 return
                    return PathFinding.k_InfiniteCostValue;
                }
                else
                {
                    // 상하좌우 노드보단 비용이 크게 설정 
                    fCost = 1.41421356237f;
                }
            }
            else
            {
                // 상하좌우 노드의 경우 가장 빠른 비용 
                fCost = 1f;
            }

            // 위에는 그냥 위치상으로 봤을 때 상하좌우이면 1비용이라 하고, 대각일때는 양옆을 판단했다면 
            // 이건 그냥 해당 이웃노드가 갈 수 있냐 없냐를 판단하는 것임 
            // 이 것을 가장 먼저 체크하는게 우선이라고 생각되기 때문에 위로 올린다 
            /*
            MapTileNode neighNode = GetNeighbor(neigborIdx) as MapTileNode;
            if (!_IsWallPassable(neighNode))
            {
                return PathFinding.k_InfiniteCostValue;
            }
            */

            return fCost;  
        }
        public override int GetNeighborCount() { return m_neighborIdxList.Count; }
        public override IPathNode GetNeighbor(int idx) { return m_owner.GetMapTileNode(m_neighborIdxList[idx]); }
        #endregion
    }

    public class MapPathFinding
    {

        public enum eHeuristicType
        {
            /// <summary>
            /// Very slow but guarantees the shortest path
            /// </summary>
            None,
            /// <summary>
            /// Faster than None, but does not guarantees the shortest path
            /// </summary>
            Manhattan,
            /// <summary>
            /// Faster than Manhattan but less accurate
            /// </summary>
            Diagonal
        }

        public eHeuristicType HeuristicType = eHeuristicType.Manhattan;

        /// <summary>
        /// Set if diagonal movement is allowed
        /// </summary>
        public bool AllowDiagonals = true;

        /// <summary>
        /// Max iterations to find a path. Use a value <= 0 for infinite iterations.
        /// Remember max iterations will be reached when trying to find a path with no solutions.
        /// </summary>
        public int MaxIterations 
        {
            get { return m_pathFinding.MaxIterations; }
            set { MaxIterations = m_pathFinding.MaxIterations; }

        }
        
        public bool IsComputing { get { return m_pathFinding.IsComputing; } }

        PathFinding m_pathFinding = new PathFinding();
        Dictionary<int, MapTileNode> m_dicTileNodes = new Dictionary<int, MapTileNode>(); //TODO: using this dictionary could lead to a memory problem. Use a pool of MapTileNodes instead.
        internal MapTileNode EndNode { get; private set; }

        /// <summary>
        /// Get a map tile node based on its index
        /// </summary>
        /// <param name="idx"></param>
        /// <returns></returns>
        public MapTileNode GetMapTileNode( int idx )
        {
            MapTileNode mapTileNode;
            bool wasFound = m_dicTileNodes.TryGetValue(idx, out mapTileNode);
            if(!wasFound)
            {
                mapTileNode = new MapTileNode(idx, this);
                m_dicTileNodes[idx] = mapTileNode;
            }
            return mapTileNode;
        }

        /// <summary>
        /// Return a list of path nodes from the start tile to the end tile ( Use RpgMapHelper class to get the tile index )
        /// </summary>
        /// <param name="startIdx"></param>
        /// <param name="endIdx"></param>
        /// <returns></returns>
        public LinkedList<IPathNode> GetRouteFromTo(int startIdx, int endIdx)
        {
            LinkedList<IPathNode> nodeList = new LinkedList<IPathNode>();
            if (m_pathFinding.IsComputing)
            {
                Debug.LogWarning("PathFinding is already computing. GetRouteFromTo will not be executed!");
            }
            else
            {                
                IPathNode start = GetMapTileNode(startIdx);
                EndNode = GetMapTileNode(endIdx);
                nodeList = m_pathFinding.ComputePath(start, EndNode);         //NOTE: the path is given from end to start ( change the order? )
            }
            return nodeList;
        }

        /// <summary>
        /// Return a list of path nodes from the start tile to the end tile ( Use RpgMapHelper class to get the tile index )
        /// </summary>
        /// <param name="startIdx"></param>
        /// <param name="endIdx"></param>
        /// <returns></returns>
        public IEnumerator GetRouteFromToAsync( int startIdx, int endIdx )
        {
            IPathNode start = GetMapTileNode(startIdx);
            EndNode = GetMapTileNode(endIdx);
            return m_pathFinding.ComputePathAsync(start, EndNode);
        }
    }

}
