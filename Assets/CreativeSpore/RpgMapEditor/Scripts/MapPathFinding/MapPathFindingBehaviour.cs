﻿using UnityEngine;
using System.Collections;
using CreativeSpore.RpgMapEditor;
using System.Collections.Generic;
using CreativeSpore.PathFindingLib;

namespace CreativeSpore.RpgMapEditor
{
    [RequireComponent(typeof(MovingBehaviour))]
    public class MapPathFindingBehaviour : MonoBehaviour
    {
        public Vector2 TargetPos;

        public delegate void OnComputedPathDelegate(MapPathFindingBehaviour source);
        public OnComputedPathDelegate OnComputedPath;

        public LinkedList<IPathNode> Path { get; private set; }
        public float MinDistToMoveNextTarget = 0.02f;

        MovingBehaviour m_movingBehavior;

        // Use this for initialization
        protected void Start()
        {
            m_movingBehavior = GetComponent<MovingBehaviour>();
            Path = new LinkedList<IPathNode>();
        }

        protected MapPathFinding m_mapPathFinding = new MapPathFinding();
        int m_startTileIdx;
        int m_endTileIdx;
        bool m_isUpdatePath = false;
        bool m_isComputing = false;
        LinkedListNode<IPathNode> m_curNode = null;

        //float now;

        float GetDistToTarget( AutoTile targetTile )
        {
            Vector3 target = RpgMapHelper.GetTileCenterPosition(targetTile.TileX, targetTile.TileY);
            target.z = transform.position.z;
            return (transform.position - target).magnitude;
        }

        // Update is called once per frame
        protected void Update()
        {
            // m_curNode != null 라는 의미는 탐색된 노드 리스트가 있다는 얘기 
            // 노드들을 쫙 이어주게 할라고 링크드리스트로 구현했네 
            // 링크드리스트는 삽입삭제 빈번할때만 좋고, 그냥 순차탐색하려면 스택이나 큐가 더 나을 것 같은디 
            if (m_curNode != null)
            {                
                MapTileNode curTileNode = m_curNode.Value as MapTileNode;

                // 현재 위치에 해당되는 타일 가져오기 
                AutoTile autoTile = RpgMapHelper.GetAutoTileByPosition(transform.position, 0);
                
                // 현재 위치 타일이 패스파인딩할 타일과 위치가 안맞거나 거리가 0.02보다 크다면 
                if (
                    (autoTile.TileX != curTileNode.TileX || autoTile.TileY != curTileNode.TileY) ||
                    GetDistToTarget(autoTile) > MinDistToMoveNextTarget // wait until min dist is reached
                )
                {
                    // 패스파인딩 타일까지 움직인다 
                    Vector3 vSeek = curTileNode.Position; vSeek.z = transform.position.z; // put at this object level
                    if (m_movingBehavior)
                    {
                        if (m_curNode.Next != null)
                            m_movingBehavior.Seek(vSeek);
                        else
                            m_movingBehavior.Arrive(vSeek);
                    }
                }
                else
                {
                    m_curNode = m_curNode.Next;
                }
            }

            //if (TargetPos != null) //TODO: TargetPos can't be null
            {
                int prevTileidx = m_startTileIdx;
                m_startTileIdx = RpgMapHelper.GetTileIdxByPosition(TargetPos);

                // 이전 타겟 위치와 현재 타겟 위치가 같지 않다면 패스를 재계산 할 필요가 없다 
                // 매프레임 타겟 위치가 바꼈는지 체크해서 바꼈다면 패스파인딩 계산을 다시 한다 
                m_isUpdatePath |= prevTileidx != m_startTileIdx;
                if (m_isUpdatePath && !m_isComputing)
                {
                    m_isUpdatePath = false;
                    // 가만보니 Start, End가 반대로 되야하는거 아닌가 
                    // Start가 현재 위치, End가 타겟 위치가 되어야 하는데 이걸 반대로 해놓네 
                    m_endTileIdx = RpgMapHelper.GetTileIdxByPosition(transform.position);
                    //now = Time.realtimeSinceStartup;

                    // Stop Start 코루틴 신공은 나도 많이 해봐서 안다
                    // 만약 이전 ComputePath가 한번에 끝나지 않고 대기상태에 존재한다면 코루틴이 점점 쌓여나간다 
                    // 이 코드는 좋은 코드가 아니다 
                    // 코루틴을 불렀으면 아예 한번에 끝나던가, 딱 한번만 코루틴을 키게 하는게 좋다 
                    StopCoroutine("ComputePath");
                    StartCoroutine("ComputePath");
                }
            }

            //+++ Debug
            for (LinkedListNode<IPathNode> it = Path.First; it != null; it = it.Next)
            {
                MapTileNode mapTileNode0 = it.Value as MapTileNode;
                if (it.Next != null)
                {
                    MapTileNode mapTileNode1 = it.Next.Value as MapTileNode;
                    Vector3 v0 = mapTileNode0.Position;
                    Vector3 v1 = mapTileNode1.Position;
                    v0.z = transform.position.z;
                    v1.z = transform.position.z;
                    Debug.DrawLine(v0, v1, Color.red);
                }
            }
            //---
        }

        IEnumerator ComputePath()
        {
            m_isComputing = true;

            // GetRouteFromToAsync 코루틴이 끝날때까지 계속 기다리네
            IEnumerator coroutine = m_mapPathFinding.GetRouteFromToAsync(m_startTileIdx, m_endTileIdx);
            while (coroutine.MoveNext()) yield return null;
            //Debug.Log("GetRouteFromToAsync execution time(ms): " + (Time.realtimeSinceStartup - now) * 1000);


            PathFinding.FindingParams findingParams = (PathFinding.FindingParams)coroutine.Current;
            Path = findingParams.computedPath;
            //+++find closest node and take next one if possible
            m_curNode = Path.First;
            if( m_curNode != null )
            {
                Vector3 vPos = transform.position; vPos.z = ((MapTileNode)m_curNode.Value).Position.z; // use same z level                
                while (m_curNode != null && m_curNode.Next != null)
                {                    
                    MapTileNode n0 = m_curNode.Value as MapTileNode;
                    MapTileNode n1 = m_curNode.Next.Value as MapTileNode;
                    float distSqr = (vPos - n0.Position).sqrMagnitude;
                    float distSqr2 = (vPos - n1.Position).sqrMagnitude;
                    if (distSqr2 < distSqr) 
                        m_curNode = m_curNode.Next;
                    else 
                        break;
                }
                // take next one, avoid moving backward in the path
                if (m_curNode.Next != null)
                    m_curNode = m_curNode.Next;
            }
            //---
            m_isComputing = false;
            if (OnComputedPath != null)
            {
                OnComputedPath(this);
            }
            yield return null;
        }
    }
}