﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace CreativeSpore.PathFindingLib
{
    public class IPathNode
    {

        public virtual bool IsPassable() { return false; }        
        public virtual int GetNeighborCount() { return 0; }
        public virtual IPathNode GetNeighbor(int idx) { return null; }
        public virtual float GetNeigborMovingCost(int neigborIdx) { return 1f; }
        public virtual float GetHeuristic() { return 0f; } //H

        internal IPathNode ParentNode = null;
        internal float Cost = 0f; //G
        internal float Score = 0f; //F

        // 닫힌 리스트에 있는건지, 열린 리스트에 있는건지를 체크하는 변수
        // 일일히 리스트를 뒤져가며 있네없네를 검색하는 것보다 변수체크하나만으로 빠르게 알 수 있으니 괜찮은 방법인 듯
        // tick이라 이름 짓고 int형을 사용한 이유는 맵 노드의 상태가 영원불멸하지 않기 때문임
        // 맵 노드상태는 언제든 바뀔 수 있다 물론 안 바뀐다면 패스파인딩 노드 비용 계산을 다시 할 필요조차 없다
        // 그게 아니기 때문에 패스파인딩 비용 재계산을 요청하면 다시 계산 해줘야 한다
        // 현재 순간의 계산인지 다른 순간의 계산인지 알기 위해 int로 한 것임
        internal int openTicks;
        internal int closeTicks;
    };

    //ref: http://www.policyalmanac.org/games/aStarTutorial.htm
    public class PathFinding
    {
        const int k_IterationsPerProcessChunk = 10; // how many iterations before returning the coroutine
        public const float k_InfiniteCostValue = float.MaxValue; // use this value to define an infinite cost

        public class FindingParams
        {
            public IPathNode startNode;
            public IPathNode endNode;
            public LinkedList<IPathNode> computedPath;
        }

        public int MaxIterations = 8000; // <= 0, for infinite iterations
        public bool IsComputing { get; private set; }

        LinkedList<IPathNode> m_openList = new LinkedList<IPathNode>();
        LinkedList<IPathNode> m_closeList = new LinkedList<IPathNode>();

        public LinkedList<IPathNode> ComputePath(IPathNode startNode, IPathNode endNode)
        {
            FindingParams findingParams = new FindingParams()
            {
                startNode = startNode,
                endNode = endNode,
                computedPath = new LinkedList<IPathNode>()
            };
            IEnumerator coroutine = ComputePathCoroutine(findingParams);
            while (coroutine.MoveNext());   // yield return null 작업을 안하기 때문에 한 프레임 안에서 모든걸 끝냄 
            return findingParams.computedPath;
        }

        public IEnumerator ComputePathAsync(IPathNode startNode, IPathNode endNode)
        {
            FindingParams findingParams = new FindingParams()
            {
                startNode = startNode,
                endNode = endNode,
                computedPath = new LinkedList<IPathNode>()
            };
            
            return ComputePathCoroutine(findingParams);
        }

        public IEnumerator ComputePathCoroutine(FindingParams findingParams)
        {
            //NOTE: curTicks will be different for each call.
            // if openTicks == curTicks, it means the node in in openList, same for closeList and closeTicks.
            // this is faster than reset a bool isInOpenList for all nodes before next call to this method
            int curTicks = (int)(Time.realtimeSinceStartup * 1000);

            IsComputing = true;

            // 시작 노드, 끝 노드가 같다면 패스파인딩 할 필요가 없음
            if (findingParams.startNode == findingParams.endNode)
            {
                findingParams.computedPath.AddLast(findingParams.startNode);
            }
            else
            {

                //1) Add the starting square (or node) to the open list.
                m_closeList.Clear();
                m_openList.Clear();
                m_openList.AddLast(findingParams.startNode); findingParams.startNode.openTicks = curTicks;

                //2) Repeat the following:
                LinkedListNode<IPathNode> curNode;
                int iterations = 0;
                int iterChunkCounter = k_IterationsPerProcessChunk;
                do
                {
                    ++iterations;

                    // k_IterationsPerProcessChunk 값은 10이다
                    // 한 프레임에 iteration 10번씩만 돌리도록 하려는 것 같다 
                    --iterChunkCounter;
                    if (iterChunkCounter == 0)
                    {
                        iterChunkCounter = k_IterationsPerProcessChunk;
                        yield return null;
                    }

                    //a) Look for the lowest F cost square on the open list. We refer to this as the current square.
                    //curNode = m_vOpen.First(c => c.Score == m_vOpen.Min(c2 => c2.Score));
                    // 일단 처음에 들어오면 열린 리스트에 시작 노드 달랑 하나만 있게 됨 
                    // 시작은 목적지 위치의 타일부터 시작함 
                    curNode = null;
                    for (LinkedListNode<IPathNode> pathNode = m_openList.First; pathNode != null; pathNode = pathNode.Next)
                    {
                        // Score가 가장 낮은 녀석으로 추려낸다 
                        if (curNode == null || pathNode.Value.Score < curNode.Value.Score)
                        {
                            curNode = pathNode;
                        }
                    }

                    //b) Switch it to the closed list.
                    // 선택된 현재 노드를 열린리스트에서 빼고 닫힌 리스트에 넣음  
                    m_openList.Remove(curNode); curNode.Value.openTicks = 0;
                    m_closeList.AddLast(curNode); curNode.Value.closeTicks = curTicks;

                    //c) For each of the 8 squares adjacent to this current square …
                    for (int i = 0; i < curNode.Value.GetNeighborCount(); ++i)
                    {
                        //If it is not walkable or if it is on the closed list, ignore it. Otherwise do the following.           
                        // 닫힌 리스트에 없고, 갈수 있는 노드일 경우 
                        IPathNode neigborNode = curNode.Value.GetNeighbor(i);
                        float movingCost = curNode.Value.GetNeigborMovingCost(i);
                        if (
                            neigborNode.closeTicks != curTicks && // if closeList does not contains node
                            movingCost != k_InfiniteCostValue && neigborNode.IsPassable()
                           ) 
                        {
                            //If it isn’t on the open list, add it to the open list. Make the current square the parent of this square. Record the F, G, and H costs of the square. 
                            float newCost = curNode.Value.Cost + movingCost;
                            // 열린 리스트에 없는 경우 
                            if (neigborNode.openTicks != curTicks) // if openList does not contains node
                            {
                                m_openList.AddLast(neigborNode); neigborNode.openTicks = curTicks;
                                neigborNode.ParentNode = curNode.Value;

                                // 패스파인딩 계산을 다 끝마친다음 Cost를 초기화하질 않네
                                // 이러면 패스파인딩이 누적될수록 최단 경로를 검색하는게 아니라 가장 덜 계산된 노드가 경로에 포함되어질 가능성이 높아진다 
                                neigborNode.Cost = newCost;

                                // 이 Heuristic이 관건 
                                // Heuristic을 주지 않으면 모든 이웃노드들을 쌩으로 계산하면서 End까지 도달한다
                                // 하지만 이 Heuristic이 목표 지점보다 가까울수록 낮은 스코어를 받게해서 더 빠르게 경로 검색이 되도록 해준다 
                                neigborNode.Score = neigborNode.Cost + neigborNode.GetHeuristic();

                                // 타겟 위치서부터 이웃 노드들의 비용들을 계산해가며 결국 시작 위치까지 도달했다면 비용계산 종료
                                if (neigborNode == findingParams.endNode)
                                {
                                    curNode.Value = neigborNode;
                                    m_openList.Clear(); // force to exit while
                                    break;
                                }
                            }
                            //If it is on the open list already, check to see if this path to that square is better, using G cost as the measure. A lower G cost means that this is a better path. 
                            // 열린 리스트에 이미 있지만 비용이 기존보다 더 작아진 경우라면 
                            else if (newCost < neigborNode.Cost)
                            {
                                //If so, change the parent of the square to the current square, and recalculate the G and F scores of the square. 
                                neigborNode.ParentNode = curNode.Value;
                                neigborNode.Cost = newCost;
                                neigborNode.Score = neigborNode.Cost + neigborNode.GetHeuristic();
                            }
                        }
                    }
                }
                while (m_openList.Count > 0 && (MaxIterations <= 0 || iterations < MaxIterations));


                //Debug.Log("iterations: " + iterations);
                //d) Stop when you:
                //Add the target square to the closed list, in which case the path has been found (see note below), or
                //Fail to find the target square, and the open list is empty. In this case, there is no path.   
                if (curNode.Value == findingParams.endNode)
                {
                    //3) Save the path. Working backwards from the target square, go from each square to its parent square until you reach the starting square. That is your path.             
                    findingParams.computedPath.AddLast(curNode.Value);
                    do
                    {
                        // 결국 이 작업을 위해 start, end을 바꿨다고 할 수 있음
                        // AddLast가 아니라 AddFirst로 바꿔도 됐을터인데 말이지 
                        curNode.Value = curNode.Value.ParentNode;
                        findingParams.computedPath.AddLast(curNode.Value);
                    }
                    while (curNode.Value != findingParams.startNode);
                }
            }
            IsComputing = false;
            yield return findingParams;
        }
    }
}