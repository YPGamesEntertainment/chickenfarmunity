﻿using UnityEngine;
using System.Collections;

public class AutoInactivate : MonoBehaviour
{
    [SerializeField]
    float _lifeTime = 1.0f;

    // unity functions ---------------------------------------------------
    void OnEnable()
    {
        StartCoroutine(ReserveInactive());
    }

    IEnumerator ReserveInactive()
    {
        yield return new WaitForSeconds(_lifeTime);

        gameObject.SetActive(false);
    }
}
