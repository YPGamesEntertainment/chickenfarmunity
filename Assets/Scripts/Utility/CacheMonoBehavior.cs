﻿using UnityEngine;
using System.Collections;

public abstract class CacheMonobehavior : MonoBehaviour
{
    private Transform _transform;
    public new Transform transform { get { return _transform ? _transform : (_transform = base.transform); } }

    private GameObject _gameObject;
    public new GameObject gameObject { get { return _gameObject ? _gameObject : (_gameObject = base.gameObject); } }

    private Animation _animation;
    public new Animation animation { get { return _animation ? _animation : (_animation = GetComponent<Animation>()); } }

    private Animator _animator;
    public Animator animator { get { return _animator ? _animator : (_animator = GetComponent<Animator>()); } }

    private AudioSource _audioSource;
    public AudioSource audioSource { get { return _audioSource ? _audioSource : (_audioSource = GetComponent<AudioSource>()); } }

    private Camera _camera;
    public new Camera camera { get { return _camera ? _camera : (_camera = GetComponent<Camera>()); } }

    private Collider _collider;
    public new Collider collider { get { return _collider ? _collider : (_collider = GetComponent<Collider>()); } }

    private Collider2D _collider2D;
    public new Collider2D collider2D { get { return _collider2D ? _collider2D : (_collider2D = GetComponent<Collider2D>()); } }

    private BoxCollider2D _boxCollider2D;
    public BoxCollider2D boxCollider2D { get { return _boxCollider2D ? _boxCollider2D : (_boxCollider2D = GetComponent<BoxCollider2D>()); } }

    private ConstantForce _constantForce;
    public new ConstantForce constantForce { get { return _constantForce ? _constantForce : (_constantForce = GetComponent<ConstantForce>()); } }

    private GUIText _guiText;
    public new GUIText guiText { get { return _guiText ? _guiText : (_guiText = GetComponent<GUIText>()); } }

    private GUITexture _guiTexture;
    public new GUITexture guiTexture { get { return _guiTexture ? _guiTexture : (_guiTexture = GetComponent<GUITexture>()); } }

    private HingeJoint _hingeJoint;
    public new HingeJoint hingeJoint { get { return _hingeJoint ? _hingeJoint : (_hingeJoint = GetComponent<HingeJoint>()); } }

    private Light _light;
    public new Light light { get { return _light ? _light : (_light = GetComponent<Light>()); } }

    private NetworkView _networkView;
    public new NetworkView networkView { get { return _networkView ? _networkView : (_networkView = GetComponent<NetworkView>()); } }

    private ParticleEmitter _particleEmitter;
    public new ParticleEmitter particleEmitter { get { return _particleEmitter ? _particleEmitter : (_particleEmitter = GetComponent<ParticleEmitter>()); } }

    private ParticleSystem _particleSystem;
    public new ParticleSystem particleSystem { get { return _particleSystem ? _particleSystem : (_particleSystem = GetComponent<ParticleSystem>()); } }

    private Renderer _renderer;
    public new Renderer renderer { get { return _renderer ? _renderer : (_renderer = GetComponent<Renderer>()); } }

    private Rigidbody _rigidbody;
    public new Rigidbody rigidbody { get { return _rigidbody ? _rigidbody : (_rigidbody = GetComponent<Rigidbody>()); } }

    private Rigidbody2D _rigidbody2D;
    public new Rigidbody2D rigidbody2D { get { return _rigidbody2D ? _rigidbody2D : (_rigidbody2D = GetComponent<Rigidbody2D>()); } }
}


